﻿using RegisterManager.Facade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RegisterManager.WebForm
{
    public class WebUtil
    {
        private const string CONTEXT_FACADE = "TCL.Facade.TheFacade";
        private readonly HttpContext _Context = HttpContext.Current;

        public WebUtil() { }

        public TheFacade Facade
        {
            get
            {
                var facade = _Context.Items[CONTEXT_FACADE] as TheFacade;
                if (null == facade)
                {
                    facade = new TheFacade();
                    _Context.Items[CONTEXT_FACADE] = facade;
                }
                return facade;
            }
        }
    }
}