﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RegisterManager.Facade
{
    public class TheFacade : IDisposable, System.ServiceModel.IExtension<System.ServiceModel.OperationContext>
    {
        public TheFacade()
        {

        }

        public UserFacade UserFacade
        {
            get
            {
                return new UserFacade();
            }
        }
        /// <summary>
        /// IDisposable Members
        /// </summary>
        public void Dispose()
        {
            
        }

        /// <summary>
        ///  IExtension<OperationContext> Members
        /// </summary>
        /// <param name="owner"></param>
        public void Attach(System.ServiceModel.OperationContext owner) { }
        public void Detach(System.ServiceModel.OperationContext owner) { }
    }
}
