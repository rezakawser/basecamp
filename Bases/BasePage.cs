﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RegisterManager.WebForm.Bases
{
    public class BasePage : System.Web.UI.Page
    {
        public BasePage() { }

        private WebUtil __Util;

        protected WebUtil _Util
        {
            get
            {
                if (null == __Util)
                    __Util = new WebUtil();
                return __Util;
            }
        }
    }
}